import { SegmentedControl, Container } from '@mantine/core';
import {
    IconBellRinging,
    IconLogout,
    IconSwitchHorizontal
} from '@tabler/icons-react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState } from 'react';
import classes from './Layout.module.css';

const tabs = {
    admin: [
        { link: '/admin', label: 'Hội sách', icon: IconBellRinging },
    ],
    customer: [
        { link: '/customer', label: 'Hội sách', icon: IconBellRinging },
    ],
};

export default function Layout({ children }) {
    const router = useRouter();
    console.log(router.route.split("/")[1]);
    const [section, setSection] = useState(router.route.split("/")[1] ?? "admin");

    const links = tabs?.[section]?.map((item) => (
        <Link
            className={classes.link}
            data-active={router.route === item.link ? true : null}
            href={item.link}
            key={item.label}
        >
            <item.icon className={classes.linkIcon} stroke={1.5} />
            <span>{item.label}</span>
        </Link>
    ));

    return (
        <main className={classes.main}>
            <nav className={classes.navbar}>
                <div>

                    <SegmentedControl
                        value={section}
                        onChange={(value) => setSection(value)}
                        transitionTimingFunction="ease"
                        fullWidth
                        data={[
                            { label: 'Admin', value: 'admin' },
                            { label: 'Customer', value: 'customer' },
                        ]}
                    />
                </div>

                <div className={classes.navbarMain}>{links}</div>

                <div className={classes.footer}>
                    <a href="#" className={classes.link} onClick={(event) => event.preventDefault()}>
                        <IconSwitchHorizontal className={classes.linkIcon} stroke={1.5} />
                        <span>Change account</span>
                    </a>

                    <a href="#" className={classes.link} onClick={(event) => event.preventDefault()}>
                        <IconLogout className={classes.linkIcon} stroke={1.5} />
                        <span>Logout</span>
                    </a>
                </div>
            </nav>
            <div style={{
                flex: '1'
            }}>
                <Container size={'lg'} py={10}>
                    {children}
                </Container>
            </div>
        </main>
    );
}