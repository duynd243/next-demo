import { Grid, Select } from '@mantine/core';
import { useQuery } from '@tanstack/react-query';
import { useState } from 'react';
import { Card } from '@/components/Card';
import axios from 'axios';
export const campaignStatuses = [{
    id: 1,
    label: 'Sắp diễn ra'
}, {
    id: 2,
    label: 'Đang diễn ra'
}, {
    id: 3,
    label: 'Đã kết thúc'
}];

export function getStatusByLabel(label) {
    return campaignStatuses.find(s => s.label === label);
}

export function getStatusLabel(status) { return campaignStatuses.find(s => s.id === status)?.status; }
const Customer = () => {
    const [statusLabel, setStatusLabel] = useState(campaignStatuses[0]?.label);
    const status = getStatusByLabel(statusLabel);
    const { data: campaigns } = useQuery({
        queryKey: ['campaigns', status.id],
        queryFn: async () => {
            const res = await axios.get('/campaigns.json');
            const filtered = res.data.filter(c => c.status === status?.id);
            return filtered
        }
    })

    return (
        <div>
            <Select
                maw={300}
                value={statusLabel}
                onChange={setStatusLabel}
                placeholder="Pick value"
                data={campaignStatuses?.map(c => c?.label)}
            />
            <Grid gutter={20} mt={12}>
                {campaigns?.map(c => (
                    <Grid.Col key={c.id} span={4}>
                        <Card campaign={c} />
                    </Grid.Col>
                ))}
            </Grid>
        </div>
    )
}

export default Customer